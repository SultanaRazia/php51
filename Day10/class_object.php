<html>
<head>
    <title> class and object method</title>
</head>
<body>
<p>
    <?php
    class person
    {
        public $isAvail = true;


        function __construct($name)
        {
            $this->name = $name;
        }

        public function dance()
        {
            return "I'm dancing!";

        }
    }
        $me = new person("Shane");
    if (is_a($me,"person")) {
        echo "I am a person, ";
    }
    if (property_exists($me,"name")) {
        echo "I have a name, ";
    }
    if (method_exists($me,"dance")){
        echo "and I know how to dance!";


    }
    ?>
</p>
</body>
</html>