<?php
$var = '';
// This will evaluate to true so the text will be printed.
if (isset($var)){
    echo "This var is so I will print.";
}

// In the next example we will use var_dump to output
//the return value of isset().

$a="test";
$b="anothertest";

var_dump(isset($a)); //true
var_dump(isset($a,$b)); //true

unset($a);

var_dump(isset($a)); //false
var_dump(isset($a,$b)); //FALSE

$foo = NUll;
var_dump(isset($foo)); //FALSE

?>