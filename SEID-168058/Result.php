<?php

session_start();

$file = fopen("studentresult.txt", "r");
if ($file) {
    while (($line = fgets($file)) !== false) {
        $data = explode(" ",$line);
        $student[$data[0]] = $data[1];
    }
    function average($data){
        $total = 0;
        foreach ($data as $value) {
            $total += floatval($value);
        }

        return $total / count($data);
    }

    function checkAboveEighty($data){
        $i = 0;
        foreach ($data as $value) {

            if ($value >= 80) {
                $i++;
            }
        }
        return $i;
    }

    function checkStudent($id,$data){
        foreach ($data as $key => $value) {
            if ($key == $id) {
                return true;
            }
        }
        return false;
    }

    if (isset($_POST['result'])) {
        echo "Average Score is : " .average($student)."<br>";

        echo "Students scored greater or equal to 80 is : ".checkAboveEighty($student)."<br>";


        $id = (isset($_POST['id']) && $_POST['id'] != "") ? $_POST['id'] : "";
        if ($id != "") {

            if (checkStudent($id,$student)) {

                foreach ($student as $key => $value) {

                    if ($key == $id) {
                        echo "Student ID : " . $key."<br>";
                        if ($value >= 90) {
                            echo "Result(Grade) : A+";
                        }else if($value >= 70 && $value <= 89){
                            echo "Result(Grade) : B+";
                        }else if($value >= 50 && $value <= 69){
                            echo  "Result(Grade) : C+";
                        }else if($value < 50){
                            echo "Result(Grade) : F";
                        }
                    }
                }

            }else{
                $_SESSION['error'] = "Student is not found.";
                header("Location: studentID.php");
            }

        }else{

            $_SESSION['error'] = "Student ID must be not empty";
            header("Location: studentID.php");
        }
    }

    fclose($file);
}
                ?>